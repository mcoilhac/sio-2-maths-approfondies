---
author: Mireille Coilhac
title: Prérequis - fonctions
tags:
    - fonctions
---

## Généralités sur les fonctions

[Généralités sur les fonctions - Prérequis](a_telecharger/generalites_fonctions.pdf){ .md-button target="_blank" rel="noopener" }

[Exercices : Généralités sur les fonctions](a_telecharger/generalites_fcts_sujet.pdf){ .md-button target="_blank" rel="noopener" }

[Correction des exercices : Généralités sur les fonctions](a_telecharger/generalites_fonctions_correction.pdf){ .md-button target="_blank" rel="noopener" }



## Prérequis sur la dérivation

[Skieur géogébra](https://www.geogebra.org/m/wemqzb3y){ .md-button target="_blank" rel="noopener" }

[Dérivation - Prérequis](a_telecharger/Derivation.pdf){ .md-button target="_blank" rel="noopener" }

[Exercices : formules de dérivation](https://coopmaths.fr/alea/?uuid=ec088&id=1AN14-3&n=5&d=10&s=6&s2=true&s3=false&alea=wu0A&cd=1&uuid=a83c0&id=1AN14-4&n=6&d=10&s=5&alea=UCY4&cd=1&uuid=1a60f&id=1AN14-5&n=5&d=10&s=6&alea=BUiw&cd=1&uuid=b32f2&id=1AN14-6&alea=naqO&uuid=3391d&id=1AN14-7&alea=wckV&v=eleve&es=0111001&title=){ .md-button target="_blank" rel="noopener" }

[Exercices : Dérivation : exercices de base](a_telecharger/Derivation_exos_revision_rectif.pdf){ .md-button target="_blank" rel="noopener" }


[Correction : Dérivation : exercices de base](a_telecharger/derivation_exos_revision_corr.pdf){ .md-button target="_blank" rel="noopener" }

## Etude d'une fonction polynôme du troisième degré

<iframe width="560" height="315" src="https://www.youtube-nocookie.com/embed/qrj3JzAriw8?si=JpahMOcHJnmKIZ_X" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" referrerpolicy="strict-origin-when-cross-origin" allowfullscreen></iframe>


[Etude de fonctions polynômes du 3ème degré](https://coopmaths.fr/alea/?uuid=e1890&id=1AN20-4&n=5&d=10&s=4&cd=1&v=eleve&es=0211001&title=){ .md-button target="_blank" rel="noopener" }


## Etude d'une fonction polynôme de degré 4

<iframe width="560" height="315" src="https://www.youtube-nocookie.com/embed/6IVxQ6piC1c?si=LbGanWHdStc7yQEr" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" referrerpolicy="strict-origin-when-cross-origin" allowfullscreen></iframe>


## Cours complet sur les études de fonctions

<iframe width="560" height="315" src="https://www.youtube-nocookie.com/embed/2Owr18MregQ?si=Oyrtkeb2WUVYZAqW" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" referrerpolicy="strict-origin-when-cross-origin" allowfullscreen></iframe>

## Calculatrices graphiques et fonctions

[Calculatrices](https://mcoilhac.forge.apps.education.fr/prerequis-maths-premiere/calculatrice/calculatrices/){ .md-button target="_blank" rel="noopener" }


## Crédits

D'après des documents de Cédric Pierquet.