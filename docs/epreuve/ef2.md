---
author: Mireille Coilhac
title: Épreuve au BTS
tags:
    - épreuve
---

_Arrêté du 8 juillet 2024 modifiant l'arrêté du 29 avril 2019 modifié_

!!! info "ÉPREUVE FACULTATIVE 2"

    Mathématiques approfondies : EPREUVE FACULTATIVE 2  
    Epreuve écrite   
    Durée 2 heures   


## 1. Finalités et objectifs

Cette épreuve vise à évaluer les compétences acquises dans le cadre de l'unité UF2 : Mathématiques approfondies.

Elle a pour objectifs :


* d'apprécier l'étendue des connaissances de la personne candidate et ses capacités à les mettre en œuvre ;
* de vérifier son aptitude au raisonnement et ses capacités à analyser correctement un problème, à justifier l'emploi des méthodes utilisées et à interpréter les résultats ;
* d'apprécier les capacités mises en œuvre dans le domaine de l'expression écrite et de l'utilisation des outils de calcul ou de représentation graphique.


## 2. Contenu

L'épreuve s'appuie sur un sujet comportant deux ou trois exercices couvrant une partie significative des modules d'analyse et des modules de probabilités et statistiques de l'unité de mathématiques approfondies, pour un total de 20 points.

## 3. Critères d'évaluation

Les compétences attendues sont évaluées sur la base des critères suivants :

* maîtrise des connaissances figurant au programme ;
* pertinence des sources d'information mobilisées ;
* efficacité dans la mise en œuvre de cette stratégie ;
* rigueur et pertinence dans l'utilisation des savoir-faire figurant au programme de mathématiques ;
* cohérence de l'argumentation employée ;
* rigueur et pertinence dans l'analyse d'un résultat ;
* qualité de la production écrite.


## 4. Modalités d'évaluation

Cette épreuve se passe uniquement sous forme ponctuelle.  
L'emploi de la calculatrice est autorisé. En fonction des besoins et afin de ne pas introduire de discriminations liées aux performances de la calculatrice employée, certaines formules de base peuvent être rappelées en tête du sujet (relations fonctionnelles, suites arithmétiques et géométriques, etc.).  
La correction est assurée par un professeur de mathématiques enseignant en STS Services informatiques aux organisations.

