---
author: Mireille Coilhac
title: Fonction exponentielle - Exercices
tags:
    - fonctions
    - exponentielle
---

## Fiche 1


[Fiche 1](https://coopmaths.fr/alea/?uuid=e3c_2020_00_sujet22_1&uuid=e3c_2020_00_sujet19_1&uuid=e3c_2021_01_specimen2_4&uuid=e3c_2021_01_specimen3_2&uuid=e3c_2020_00_sujet1_2&uuid=e3c_2020_00_sujet3_3&uuid=e3c_2020_00_sujet23_2&uuid=e3c_2020_00_sujet24_2&v=eleve&es=0111001&title=){ .md-button target="_blank" rel="noopener" }


## Fiche 2

Cette fiche doit vous aider à acquérir des automatismes pour dériver des fonctions 
avec la fonction exponentielle.

Attention, actuellement il y a une petite inexactitude dans la rédation des corrections : 
Soit on ne parle que des fonctions, et la variable $x$ n'intervient nulle part, 
soit on parle des images d'un réel $x$, et dans ce cas-là, la variable $x$ doit se 
trouver des deux côtés du signe $=$.  
Par exemple ~~$u= \text{e}^x$~~  doit être remplacé par $u(x)= \text{e}^x$

[Fiche 2 : automatismes de dérivation](https://xymaths.fr/Lycee/Exercices-Corriges-Calcul-Derivees-Exponentielle/#f1){ .md-button target="_blank" rel="noopener" }

