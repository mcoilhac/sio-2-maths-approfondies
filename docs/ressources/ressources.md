---
author: Mireille Coilhac
title: 🎁 Ressources
---


[SIO 1 - maths approfondies](https://mcoilhac.forge.apps.education.fr/sio-1-maths-approfondies){ .md-button target="_blank" rel="noopener" }

[Quelques bases très basiques](https://mcoilhac.forge.apps.education.fr/prerequis-maths-premiere/){ .md-button target="_blank" rel="noopener" }

[Utiliser les calculatrices graphiques pour les fonctions](https://mcoilhac.forge.apps.education.fr/prerequis-maths-premiere/calculatrice/calculatrices/){ .md-button target="_blank" rel="noopener" }


## Unité de mathématiques approfondies UF2

L’objectif de l’enseignement de mathématiques approfondies est de préparer la personne étudiante à d’éventuelles poursuites d’études et à la familiariser aux calculs correspondants avec sa calculatrice et d’autres moyens informatiques, et à interpréter les résultats ainsi obtenus. À nouveau, l’utilisation de moyens informatiques (calculatrice, ordinateur) est recommandée pour faciliter la compréhension de concepts par des illustrations graphiques et numériques et pour les calculs non élémentaires. 

Les exemples et exercices reposent sur des situations de vie courante ou issues des autres disciplines. La compréhension d’une modélisation et son interprétation seront jugées plus importantes qu’une agilité calculatoire. 

Le programme est constitué des modules suivants décrits par le programme de mathématiques des brevets de technicien supérieur (arrêté du 4 juin 2013) : 

* Suites numériques ; 
* Fonctions d’une variable réelle, à l’exception de l’item « Fonctions sinus et cosinus » et des paragraphes « Approximation locale d’une fonction » et « Courbes paramétrées » ; 
* Calcul intégral, à l’exception de l’item « Complément : primitives de t $\mapsto$ cos(ωt  +  ϕ) et t $\mapsto$ sin(ωt  +  ϕ) » du paragraphe « Primitives » et de l’item « Formule d’intégration par parties » du paragraphe « Intégration » ; 
* Statistique descriptive ; 
* Probabilités 1, à l’exception de l’item « Théorème de la limite centrée » ; 
* Probabilités 2, à l’exception du paragraphe « Exemples de processus aléatoires ». 

[Les modules UF2](https://nuage02.apps.education.fr/index.php/s/aGwPPJPPMeD4ySj){ .md-button target="_blank" rel="noopener" }