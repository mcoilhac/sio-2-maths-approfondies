---
author: Mireille Coilhac
title: Statistiques à une variable
tags:
    - statistiques
---

[Statistiques à une variable](https://mcoilhac.forge.apps.education.fr/sio-1-maths-approfondies/stats/stats_1/){ .md-button target="_blank" rel="noopener" }